// SECTION MongoDb Aggregation
 

 /*
     -to generate and perform operations to create filtered results that help us
     analyze the data

 */


/*
    using aggregate method:
    -the "$match" is used to pass the documents that meet
    the specified conditon
    /condition to the next stage or aggregation process

    syntax:
    {$match : {field:value}}
*/

db.fruits.aggregate([
	{
		$match:{onSale: true
	}
}])

/*
   - the group is used to group the elements
   together and field-value the data from the grouped element


   syntax:

   {$group: _id: "fieldSetGroup"}

*/

db.fruits.aggregate([
     {
     	$match: {
          color: "Yellow"
     	}
     },
     {
     	$group: {
     	 _id:"$supplier_id", totalFruits: {$sum: "$stock"}
     	}
     }

	])


// MIni activity
// First get all the fruites that color yellow and group theme into their respective supplier and get avaialasble stocks

db.fruits.aggregate([{
	$match: {
		color: "$Yellow"
	}
},
{
	$group: {
		_id: "$supplier_id", totalFruits:{$sum: "$stock"}
	}
}
])

//field projection with aggregation

/*
     -$prokect can be used when aggregation data to include /
     exlcude from the returned result

     syntax:
     {$project : {Field:1/0}}
*/


db.fruits.aggregate([
	{
		$match:{onSale: true
	}
},
  {
     	$group: {
     	 _id:"$supplier_id", totalFruits: {$sum: "$stock"}
     	}
     },
     {
     	$project: {
     		_id:0
     	}
     }

	])

// Sorting aggregated results
   /*
     -$sort can be used to change the order of the aggregated
     result


   */


db.fruits.aggregate([
	{
		$match:{onSale: true
	}
},
  {
     	$group: {
     	 _id:"$supplier_id", totalFruits: {$sum: "$stock"}
     	}
     },
      {
     	$project: {
     		_id:0
     	}
     }
     ,
     {
     	$sort: {
     		totalFruits: 1
     	}
     }

	])

/*
   the value in sort

    1  - lowes to highest
   -1 - highest to lowers

*/

//aggreagating results based on an array fields
/*
     the $unwind deconstructs an array field from a collection/
     field with an array value to output a result

*/

db.fruits.aggregate([
      {
      	$unwind: "$origin"
      },
      {
      	$group: {_id:"$origin", fruits:{$sum: 1}}
      }
	])

//[SeCTION]Other aggregate stages
//$count all yellow fruits

db.fruits.aggregate([
     {
     	$match: {
     		color: "Yellow"
     	}
     },{
     	$count: "Yellow Fruits"
     }

	])

//average gets the average value of the stock

db.fruits.aggregate([
     {
     	$match:{
     		color: "Yellow"
     	}
     },
     {
     	$group:{
     		_id: "$color", avgYellow: {$avg: "$stock"}
     	}
     }
	])
//min and $max

db.fruits.aggregate([
     {
     	$match:{
     		color: "Yellow"
     	}
     },
     {
     	$group:{
     		_id: "$color", lowestStock: {$min: "$stock"}
     	}
     }
	])